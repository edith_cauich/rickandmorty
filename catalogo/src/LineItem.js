const LineItem = ({ item, handleDelete }) => {
  return (
    <>
      <div className="col-lg-4 py-3">
        <div className="">
          <div className="card">
            <div className="card-header fw-bolder bg-dark text-info">
              <div className="row">
                <div className="col-lg-10 d-flex align-items-center ">
                  <p className="lh-lg mb-0">Registro #{item.id}</p>
                </div>
                <div className="col-lg-2 d-flex justify-content-end line">
                  <button
                    onClick={() => handleDelete(item.id)}
                    role="button"
                    className="btn"
                    tabIndex="0"
                    aria-label={`Delete ${item.item}`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="#f9f5f5"
                      className="bi bi-x-circle"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                      <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                  </button>
                </div>
              </div>
            </div>
            <div className="card-body line">
              <div className="row">
                <div className="col-lg-12 d-flex align-items-center">
                  <p className="fw-bold mb-0 text-rey lh-lg text-info">
                    {item.name}
                  </p>
                </div>
              </div>
              <h5 className="card-title"></h5>
              <h6 className="card-subtitle mb-2 text-muted">{item.episode}</h6>
              <p className="card-text">{item.air_date}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LineItem;
