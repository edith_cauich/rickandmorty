import Content from "./Content";

import { useState, useEffect } from "react";
import apiRequest from "./apiRequest";
import AddItem from "./AddItem";

function App() {
  const API_URL = "http://localhost:3001/items";

  const [items, setItems] = useState([]);
  const [newItem, setNewItem] = useState("");
  const [newDate, setNewDate] = useState("");
  const [newEpisode, setNewEpisode] = useState("");

  //lista de datos
  useEffect(() => {
    const fetchItems = async () => {
      try {
        const response = await fetch(API_URL);
        if (!response.ok) throw Error("no esperado");
        const listItems = await response.json();
        setItems(listItems);
      } catch (err) {
        console.log(err.message);
      } finally {
        console.log(false);
      }
    };

    setTimeout(() => fetchItems(), 2000);
  }, []);

  //create
  const addItem = async (name, air_date, episode) => {
    //crear nuevo id para el dato, si existe datos en el array se le suma 1, caso contrario se le asigna 1
    const id = items.length ? items[items.length - 1].id + 1 : 1;
    //campos del form
    const myNewItem = { id, name, air_date, episode };
    //operador de extension
    const listItems = [...items, myNewItem];

    //agrega a la lista
    setItems(listItems);
    const postOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(myNewItem),
    };
    const result = await apiRequest(API_URL, postOptions);
  };

  //funcion para form
  const handleSubmit = (e) => {
    e.preventDefault();
    //si la varible no contiene datos, no se ejecuta nada
    if (!newItem) return;
    addItem(newItem, newDate, newEpisode); //agrega a la lista
    setNewItem(""); //restablece la variable newItem a una cadena vacía después de agregar
    setNewDate("");
    setNewEpisode("");
  };

  //delete
  const handleDelete = async (id) => {
    //filtra los datos excepto los eliminados
    const listItems = items.filter((item) => item.id !== id);
    //para hacer en render de la lista
    setItems(listItems);

    const deleteOptions = { method: "DELETE" };
    const reqUrl = `${API_URL}/${id}`;
    const result = await apiRequest(reqUrl, deleteOptions);
  };

  return (
    <div className="App ">
      <nav className="navbar navbar-expand-lg navbar-light bg-info">
  <div className="container-fluid">
    <a className="navbar-brand fw-bolder text-uppercase" href="#">Formulario de episodios</a>
    
    
  </div>
</nav>
      <div className="container-fluid py-5 px-5">
        <div className="row">
          <div className="col-lg-4 line pe-5">
            <AddItem
              newItem={newItem}
              newDate={newDate}
              setNewItem={setNewItem}
              setNewDate={setNewDate}
              newEpisode={newEpisode}
              setNewEpisode={setNewEpisode}
              handleSubmit={handleSubmit}
            />
          </div>
          <div className="col-lg-8 line">
            <div className="row">
              <Content
                items={items.filter((item) => item.name)}
                handleDelete={handleDelete}
              />
            </div>
          </div>
        </div>

        <div className="row"></div>
      </div>
    </div>
  );
}

export default App;
