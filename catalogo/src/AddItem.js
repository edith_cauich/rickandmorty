import { useRef } from "react";

const AddItem = ({
  newItem,
  newDate,
  setNewItem,
  setNewDate,
  newEpisode,
  setNewEpisode,
  handleSubmit,
}) => {
  const inputRef = useRef();

  return (
    <form className="addForm" onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-lg-12">
          <h4 htmlFor="addItem">Agregar nuevo registro</h4>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <input
            className="form-control my-4"
            ref={inputRef}
            id="addItem"
            type="text"
            placeholder="Nombre de episodio"
            required
            value={newItem}
            onChange={(e) => setNewItem(e.target.value)}
          />

          <input
            className="form-control my-4"
            ref={inputRef}
            id="addEpisode"
            type="text"
            placeholder="Clave"
            required
            value={newEpisode}
            onChange={(e) => setNewEpisode(e.target.value)}
          />

          <input
            className="form-control my-4"
            ref={inputRef}
            id="addDate"
            type="text"
            placeholder="Fecha al aire"
            required
            value={newDate}
            onChange={(e) => setNewDate(e.target.value)}
          />
        </div>
        <div className="col-lg-12 py-2">
          <button
            type="submit"
            className="btn btn-primary"
            aria-label="Add Item"
            onClick={() => inputRef.current.focus()}
          >
            Agregar{" "}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-plus-circle"
              viewBox="0 0 16 16"
            >
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
              <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
            </svg>
          </button>
        </div>
      </div>
    </form>
  );
};

export default AddItem;
