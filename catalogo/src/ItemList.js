import LineItem from "./LineItem";

const ItemList = ({ items, handleDelete }) => {
  return (
    <>
      {items.map((item) => (
        <LineItem key={item.id} item={item} handleDelete={handleDelete} />
      ))}
    </>
  );
};

export default ItemList;
