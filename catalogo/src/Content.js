import ItemList from "./ItemList";

const Content = ({ items, handleDelete }) => {
  return (
    <>
      <ItemList items={items} handleDelete={handleDelete} />
    </>
  );
};

export default Content;
